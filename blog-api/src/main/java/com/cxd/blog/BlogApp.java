package com.cxd.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/5 21:22
 * @Description:
 */
@SpringBootApplication
@CrossOrigin
@EnableTransactionManagement
public class BlogApp {
    public static void main(String[] args) {
        SpringApplication.run(BlogApp.class, args);
    }
}
