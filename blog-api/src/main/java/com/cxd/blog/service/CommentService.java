package com.cxd.blog.service;

import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.params.CommentParam;

/**
 * (Comment)表服务接口
 *
 * @author cxd
 * @since 2021-09-09 00:08:28
 */
public interface CommentService {

    /**
     * 根据文章ID查询评论
     *
     * @param articleId
     * @return
     */
    Result getCommentsByArticleId(Long articleId);

    /**
     * 文章评论
     *
     * @param commentParam
     * @return
     */
    Result comment(CommentParam commentParam);
}
