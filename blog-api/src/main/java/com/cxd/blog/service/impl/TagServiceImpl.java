package com.cxd.blog.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cxd.blog.mapper.TagMapper;
import com.cxd.blog.model.entity.Tag;
import com.cxd.blog.service.TagService;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.TagVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

/**
 * (Tag)表服务实现类
 *
 * @author cxd
 * @since 2021-08-05 22:51:23
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapper tagMapper;


    @Override
    public Result findAll() {
        LambdaQueryWrapper<Tag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(Tag::getId, Tag::getTagName);
        List<Tag> tagList = tagMapper.selectList(queryWrapper);
        return Result.success(BeanUtil.copyToList(tagList, TagVo.class));
    }

    @Override
    public Result findAllDetail() {
        LambdaQueryWrapper<Tag> queryWrapper = new LambdaQueryWrapper<>();
        List<Tag> tagList = tagMapper.selectList(queryWrapper);
        return Result.success(BeanUtil.copyToList(tagList, TagVo.class));
    }

    /**
     * 通过ID查询标签详情
     *
     * @param id
     * @return
     */
    @Override
    public Result findAllDetailById(Long id) {
        Tag tag = tagMapper.selectById(id);
        return Result.success(BeanUtil.copyProperties(tag, TagVo.class));
    }

    @Override
    public Result hotArticles(int limit) {
        //标签拥有的文章数量最多 最热标签
        List<Long> tagIds = tagMapper.findHotsByIds(limit);
        if (CollectionUtils.isEmpty(tagIds)) {
            return Result.success(Collections.emptyList());
        }
        List<Tag> tagList = tagMapper.findTagsByTagIds(tagIds);
        return Result.success(tagList);
    }

    @Override
    public List<TagVo> findTagByArticleId(Long articleId) {
        List<Tag> tagList = tagMapper.findTagsByArticleId(articleId);
        return BeanUtil.copyToList(tagList, TagVo.class);
    }

}
