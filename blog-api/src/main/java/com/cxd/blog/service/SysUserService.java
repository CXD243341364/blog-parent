package com.cxd.blog.service;

import com.cxd.blog.model.entity.SysUser;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.UserVo;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/6 0:43
 * @Description:
 */
public interface SysUserService {

    /**
     * 根据id查询作者
     * @param id
     * @return
     */
    SysUser findUserById(Long id);

    /**
     * 根据用户名密码查询
     * @param accountName
     * @param password
     * @return
     */
    SysUser findUser(String accountName, String password);

    /**
     * 根据请求头中token信息查询用户信息
     * @param token
     * @return
     */
    Result getUserInfoByToken(String token);

    /**
     * 通过用户名查找用户
     * @param accountName
     * @return
     */
    SysUser findUserByAccountName(String accountName);

    /**
     * 保存用户信息
     * @param sysUser
     * @return
     */
    Result save(SysUser sysUser);

    /**
     * 根据id查询UserVo用户信息
     * @param id
     * @return
     */
    UserVo findUserVoById(Long id);
}
