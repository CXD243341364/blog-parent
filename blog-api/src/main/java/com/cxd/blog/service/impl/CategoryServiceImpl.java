package com.cxd.blog.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cxd.blog.mapper.CategoryMapper;
import com.cxd.blog.model.entity.Category;
import com.cxd.blog.service.CategoryService;
import com.cxd.blog.model.vo.CategoryVo;
import com.cxd.blog.model.vo.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/7 23:59
 * @Description:
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 查询全部类别
     *
     * @return
     */
    @Override
    public Result findAll() {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(Category::getId, Category::getAvatar, Category::getCategoryName);
        List<Category> categoriesList = categoryMapper.selectList(queryWrapper);
        List<CategoryVo> categoryVoList = BeanUtil.copyToList(categoriesList, CategoryVo.class);
        return Result.success(categoryVoList);
    }

    /**
     * 通过ID查询类别
     *
     * @param categoryId
     * @return
     */
    @Override
    public CategoryVo findCategoryById(Long categoryId) {
        Category category = categoryMapper.selectById(categoryId);
        CategoryVo categoryVo = new CategoryVo();
        BeanUtils.copyProperties(category, categoryVo);
        return categoryVo;
    }

    @Override
    public Result findAllDetail() {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        List<Category> categoriesList = categoryMapper.selectList(queryWrapper);
        return Result.success(BeanUtil.copyToList(categoriesList, CategoryVo.class));
    }

    /**
     * 通过id查询分类信息
     *
     * @return
     */
    @Override
    public Result findDetailById(Long id) {
        Category category = categoryMapper.selectById(id);
        return Result.success(BeanUtil.copyProperties(category, CategoryVo.class));
    }
}
