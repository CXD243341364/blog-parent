package com.cxd.blog.service;

import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.TagVo;

import java.util.List;

/**
 * (Tag)表服务接口
 *
 * @author cxd
 * @since 2021-08-05 22:51:23
 */
public interface TagService {

    /**
     * id查找tags
     *
     * @param articleId
     * @return
     */
    List<TagVo> findTagByArticleId(Long articleId);

    /**
     * 最热标签
     *
     * @param limit
     * @return
     */
    Result hotArticles(int limit);

    /**
     * 查询全部标签
     *
     * @return
     */
    Result findAll();

    /**
     * 查询标签详情
     *
     * @return
     */
    Result findAllDetail();

    /**
     * 通过ID查询标签详情
     *
     * @param id
     * @return
     */
    Result findAllDetailById(Long id);
}
