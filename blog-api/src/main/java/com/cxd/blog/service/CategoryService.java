package com.cxd.blog.service;

import com.cxd.blog.model.vo.CategoryVo;
import com.cxd.blog.model.vo.Result;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/7 23:58
 * @Description:
 */
public interface CategoryService {

    /**
     * 根据类别ID查找类别
     *
     * @param categoryId
     * @return
     */
    CategoryVo findCategoryById(Long categoryId);

    /**
     * 查询全部类别
     *
     * @return
     */
    Result findAll();

    /**
     * 查询全部分类信息
     *
     * @return
     */
    Result findAllDetail();

    /**
     * 通过id查询分类信息
     *
     * @param id
     * @return
     */
    Result findDetailById(Long id);
}
