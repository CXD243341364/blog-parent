package com.cxd.blog.service;

import com.aliyun.oss.model.OSSObjectSummary;
import com.cxd.blog.model.vo.FileUploadResult;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/20 17:51
 * @Description:
 */
public interface FileService {
    /**
     * 上传文件
     *
     * @param file
     * @return
     */
    FileUploadResult upload(MultipartFile file);

    /**
     * 查看文件列表
     *
     * @return
     */
    List<OSSObjectSummary> list();

    /**
     * 删除文件
     *
     * @param objectName
     * @return
     */
    FileUploadResult delete(@Param("objectName") String objectName);

    /**
     * 下载文件
     *
     * @param os
     * @param objectName
     * @throws IOException
     */
    void exportOssFile(OutputStream os, String objectName) throws IOException;
}
