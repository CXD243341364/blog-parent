package com.cxd.blog.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cxd.blog.mapper.SysUserMapper;
import com.cxd.blog.model.entity.SysUser;
import com.cxd.blog.service.SysUserService;
import com.cxd.blog.utils.JWTUtils;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.ResultCode;
import com.cxd.blog.model.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/6 0:45
 * @Description:
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    private final static Logger LOGGER = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 根据token查询用户信息
     * @param token
     * @return
     */
    @Override
    public Result getUserInfoByToken(String token) {
        Map<String, Object> map = JWTUtils.checkToken(token);
        if (CollectionUtils.isEmpty(map)) {
            return Result.fail(ResultCode.TOKEN_NOT_ALLOW.getCode(), ResultCode.TOKEN_NOT_ALLOW.getMsg());
        }
        String userJson = redisTemplate.opsForValue().get("TOKEN_" + token);
        if (StringUtils.isBlank(userJson)) {
            return Result.fail(ResultCode.NO_TOKEN.getCode(), ResultCode.NO_TOKEN.getMsg());
        }
//        userJson = userJson.substring(1, userJson.length() - 1);
//        userJson = userJson.replaceAll("\\\\", "");
        SysUser sysUser = JSON.parseObject(userJson, SysUser.class);
//        LoginVo loginVo = new LoginVo();
//        loginVo.setAccount(sysUser.getAccount());
//        loginVo.setId(sysUser.getId());
//        loginVo.setNickName(sysUser.getNickname());
//        loginVo.setAvatar(sysUser.getAvatar());
        return Result.success(sysUser);
    }

    @Override
    public SysUser findUserById(Long id) {
        SysUser sysUser = sysUserMapper.selectById(id);
        if (sysUser == null) {
            sysUser = new SysUser();
            sysUser.setNickname("system_name");
        }
        return sysUser;
    }

    @Override
    public SysUser findUser(String accountName, String password) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getAccount, accountName);
        queryWrapper.eq(SysUser::getPassword, password);
        queryWrapper.select(SysUser::getAccount, SysUser::getId, SysUser::getAvatar, SysUser::getNickname);
        queryWrapper.last("limit 1");
        return sysUserMapper.selectOne(queryWrapper);
    }

    @Override
    public SysUser findUserByAccountName(String accountName) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getAccount,accountName);
        queryWrapper.last("limit 1");
        return sysUserMapper.selectOne(queryWrapper);
    }

    @Override
    public Result save(SysUser sysUser) {
        int result = sysUserMapper.insert(sysUser);
        return result > 0 ? Result.success(ResultCode.SAVE_USER_INFO_SUCCEEDED.getMsg()) : Result.fail(ResultCode.SAVE_USER_INFO_FAILED.getCode(),ResultCode.SAVE_USER_INFO_FAILED.getMsg());
    }

    @Override
    public UserVo findUserVoById(Long id) {
        SysUser sysUser = sysUserMapper.selectById(id);
        if (sysUser == null) {
            sysUser = new SysUser();
            sysUser.setId(1L);
            sysUser.setAvatar("/static/img/logo.b3a48c0.png");
            sysUser.setNickname("码神之路");
        }
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(sysUser, userVo);
        return userVo;
    }
}
