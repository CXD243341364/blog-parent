package com.cxd.blog.service;

import com.cxd.blog.model.vo.ArticleVo;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.params.ArticleParam;
import com.cxd.blog.model.vo.params.PageParams;

import java.util.List;

/**
 * (Article)表服务接口
 *
 * @author cxd
 * @since 2021-08-05 22:13:41
 */
public interface ArticleService {

    /**
     * 首页数据
     *
     * @param pageParams
     * @return
     */
    List<ArticleVo> listArticlesPage(PageParams pageParams);

    /**
     * 热门文章
     *
     * @param limit
     * @return
     */
    Result hotArticle(int limit);

    /**
     * 最新文章
     *
     * @param limit
     * @return
     */
    Result newArticle(int limit);

    /**
     * 文章归档
     *
     * @return
     */
    Result listArchives();

    /**
     * 查看文章详情
     *
     * @param id
     * @return
     */
    Result findArticleById(Long id);

    /**
     * 发布文章
     *
     * @param articleParam
     * @return
     */
    Result publish(ArticleParam articleParam);
}
