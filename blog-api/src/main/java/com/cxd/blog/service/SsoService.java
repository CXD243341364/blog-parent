package com.cxd.blog.service;

import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.params.LoginParams;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/24 23:40
 * @Description:
 */
@Transactional(rollbackFor = Exception.class)
public interface SsoService {

    /**
     * 用户登陆验证
     *
     * @param loginParams
     * @return
     */
    Result login(LoginParams loginParams);

    /**
     * 退出登陆
     *
     * @param token
     * @return
     */
    Result logout(String token);

    /**
     * 注册用户
     *
     * @param loginParams
     * @return
     */
    Result register(LoginParams loginParams);

}
