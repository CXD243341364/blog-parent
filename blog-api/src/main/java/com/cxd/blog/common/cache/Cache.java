package com.cxd.blog.common.cache;

import java.lang.annotation.*;

/**
 * @author PGTWO
 * @date 2021/9/17 16:21
 * @description 缓存注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cache {

    /**
     * 过期时间
     *
     * @return
     */
    long expire() default 1 * 60 * 1000;

    /**
     * 标识
     *
     * @return
     */
    String name() default "";

}
