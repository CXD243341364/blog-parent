package com.cxd.blog.common.aop;

import com.alibaba.fastjson.JSON;
import com.cxd.blog.utils.HttpContextUtils;
import com.cxd.blog.utils.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/14 21:47
 * @Description: 切面 定义了通知和切入点的关系
 */
@Component
@Aspect
@Slf4j
public class LogAspect {

    /**
     * 定义切点
     */
    @Pointcut("@annotation(com.cxd.blog.common.aop.OpLog)")
    public void LoginPointcut() {

    }

    @Around("LoginPointcut()")
    public Object aroundLoginPointcut(ProceedingJoinPoint point) throws Throwable {
        Long beginTime = System.currentTimeMillis();
        //执行方法
        Object result = point.proceed();
        //执行时长
        long time = System.currentTimeMillis() - beginTime;
        //保存日志
        recordLog(point,time);
        return result;
    }

    private void recordLog(ProceedingJoinPoint point,Long time){
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        OpLog opLog = method.getAnnotation(OpLog.class);
        log.info("=====================log start================================");
        log.info("module:{}",opLog.module());
        log.info("operation:{}",opLog.operation());

        //请求的方法名
        String className = point.getTarget().getClass().getName();
        String methodName = signature.getName();
        log.info("request method:{}",className + "." + methodName + "()");

        //请求的参数
        Object[] args = point.getArgs();
        String params = JSON.toJSONString(args[0]);
        log.info("params:{}",params);

        //获取request 设置IP地址
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        log.info("ip:{}", IpUtils.getIpAddr(request));


        log.info("excute time : {} ms",time);
        log.info("=====================log end================================");
    }
}
