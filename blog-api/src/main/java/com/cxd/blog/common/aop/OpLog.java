package com.cxd.blog.common.aop;

import java.lang.annotation.*;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/14 21:42
 * @Description: AOP注解切面
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpLog {

    /**
     * 模块
     *
     * @return
     */
    String module() default "";

    /**
     * 操作
     *
     * @return
     */
    String operation() default "";
}
