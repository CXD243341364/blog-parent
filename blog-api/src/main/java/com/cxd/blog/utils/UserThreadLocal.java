package com.cxd.blog.utils;

import com.cxd.blog.model.entity.SysUser;

/**
 * 每一个Thread维护一个ThreadLocalMap, key为使用**弱引用**的ThreadLocal实例，value为线程变量的副本。
 *
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/7 21:04
 * @Description: ThreadLocal保存用户信息
 */
public class UserThreadLocal {

    /**
     * 私有化方法
     */
    private UserThreadLocal() {
    }

    public static final ThreadLocal<SysUser> USER_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 保存用户信息至ThreadLocal
     *
     * @param sysUser
     */
    public static void put(SysUser sysUser) {
        USER_THREAD_LOCAL.set(sysUser);
    }

    /**
     * ThreadLocal中取出用户信息
     *
     * @return
     */
    public static SysUser get() {
        return USER_THREAD_LOCAL.get();
    }

    /**
     * 删除ThreadLocal中的用户信息
     */
    public static void remove() {
        USER_THREAD_LOCAL.remove();
    }
}
