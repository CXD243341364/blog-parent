package com.cxd.blog.handler;

import com.cxd.blog.model.vo.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/8 22:49
 * @Description: 对加了@controller注解的方法进行拦截处理 基于AOP实现
 */
@ControllerAdvice
public class AllExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result doException(Exception e) {
        e.printStackTrace();
        return Result.fail(-9999, "系统异常");
    }
}
