package com.cxd.blog.controller;

import com.alibaba.fastjson.JSON;
import com.cxd.blog.model.entity.SysUser;
import com.cxd.blog.utils.UserThreadLocal;
import com.cxd.blog.model.vo.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/26 0:13
 * @Description:
 */
@RestController
@RequestMapping("test")
public class TestController {

    @RequestMapping(method = RequestMethod.GET)
    public Result test(){
        SysUser sysUser = UserThreadLocal.get();
        return Result.success(JSON.toJSONString(sysUser));
    }
}
