package com.cxd.blog.controller;

import com.cxd.blog.service.SsoService;
import com.cxd.blog.model.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/18 22:46
 * @Description: 登出
 */
@RestController
@RequestMapping("logout")
public class LogOutController {
    @Autowired
    private SsoService ssoService;

    /**
     * 登出
     * @param token
     * @return
     */
    @GetMapping
    public Result logout(@RequestHeader("Authorization") String token) {
        return ssoService.logout(token);
    }
}
