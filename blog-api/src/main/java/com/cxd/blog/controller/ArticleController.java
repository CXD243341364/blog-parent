package com.cxd.blog.controller;


import com.cxd.blog.common.aop.OpLog;
import com.cxd.blog.common.cache.Cache;
import com.cxd.blog.model.vo.ArticleVo;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.service.ArticleService;
import com.cxd.blog.model.vo.params.ArticleParam;
import com.cxd.blog.model.vo.params.PageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * (Article)表控制层
 *
 * @author cxd
 * @since 2021-08-05 22:21:10
 */
@RestController
@RequestMapping("articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    /**
     * 首页数据
     *
     * @param pageParams
     * @return
     */
    @PostMapping
    @Cache(expire = 5 * 60 * 1000, name = "list_Article")
    @OpLog(module = "文章", operation = "获取文章列表")
    public Result articles(@RequestBody PageParams pageParams) {
        //ArticleVo 页面接收的数据
        List<ArticleVo> articles = articleService.listArticlesPage(pageParams);
        return Result.success(articles);
    }

    /**
     * 最热文章
     *
     * @return
     */
    @PostMapping("hot")
    @Cache(expire = 5 * 60 * 1000, name = "hot_Article")
    public Result hotArticle() {
        int limit = 5;
        return articleService.hotArticle(limit);
    }

    /**
     * 最新文章
     *
     * @return
     */
    @PostMapping("new")
    @Cache(expire = 5 * 60 * 1000, name = "new_Article")
    public Result newArticle() {
        int limit = 5;
        return articleService.newArticle(limit);
    }

    /**
     * 文章归档
     *
     * @return
     */
    @PostMapping("listArchives")
    public Result listArchives() {
        return articleService.listArchives();
    }

    /**
     * 查看文章详情
     *
     * @param id
     * @return
     */
    @PostMapping("view/{id}")
    @OpLog(module = "文章", operation = "查看文章详情")
    public Result findArticleById(@PathVariable("id") Long id) {
        return articleService.findArticleById(id);
    }

    /**
     * 发布文章
     *
     * @param articleParam
     * @return
     */
    @PostMapping("publish")
    public Result publish(@RequestBody ArticleParam articleParam) {
        return articleService.publish(articleParam);
    }
}
