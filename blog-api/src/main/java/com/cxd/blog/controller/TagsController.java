package com.cxd.blog.controller;

import com.cxd.blog.service.TagService;
import com.cxd.blog.model.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/8 19:47
 * @Description: 标签
 */
@RestController
@RequestMapping("/tags")
public class TagsController {

    @Autowired
    private TagService tagService;

    /**
     * 最热标签
     *
     * @return
     */
    @GetMapping("/hot")
    public Result getHot() {
        int limit = 6;
        return tagService.hotArticles(limit);
    }

    /**
     * 全部标签
     *
     * @return
     */
    @GetMapping
    public Result findAll() {
        return tagService.findAll();
    }

    @GetMapping("detail")
    public Result findAllDetail(){
        return tagService.findAllDetail();
    }

    @GetMapping("detail/{id}")
    public Result findAllDetail(@PathVariable("id") Long id){
        return tagService.findAllDetailById(id);
    }

}
