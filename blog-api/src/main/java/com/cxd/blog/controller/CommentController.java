package com.cxd.blog.controller;


import com.cxd.blog.model.vo.Result;
import com.cxd.blog.service.CommentService;
import com.cxd.blog.model.vo.params.CommentParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Comment)表控制层
 *
 * @author cxd
 * @since 2021-09-09 00:08:02
 */
@RestController
@RequestMapping("comments")
public class CommentController {
    /**
     * 服务对象
     */
    @Resource
    private CommentService commentService;

    /**
     * 文章对应评论列表
     *
     * @param articleId
     * @return
     */
    @GetMapping("article/{id}")
    public Result getComments(@PathVariable("id") Long articleId) {
        return commentService.getCommentsByArticleId(articleId);
    }

    /**
     * 文章评论
     *
     * @param commentParam
     * @return
     */
    @PostMapping("create/change")
    public Result comment(@RequestBody CommentParam commentParam) {
        return commentService.comment(commentParam);
    }


}
