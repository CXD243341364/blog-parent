package com.cxd.blog.controller;

import com.cxd.blog.model.vo.Result;
import com.cxd.blog.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/20 17:50
 * @Description:
 */
@RestController
@RequestMapping("upload")
public class FileController {

    @Autowired
    private FileService fileService;

    @PostMapping
    public Result upload(@RequestParam("image") MultipartFile file) {
        return fileService.upload(file);
    }
}
