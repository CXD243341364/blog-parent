package com.cxd.blog.controller;

import com.cxd.blog.service.SsoService;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.params.LoginParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/9 23:37
 * @Description: 登陆
 */
@RestController
@RequestMapping("login")
public class LoginController {

    @Autowired
    private SsoService ssoService;

    /**
     * 用户登陆
     * @param loginParams
     * @return
     */
    @PostMapping
    public Result login(@RequestBody LoginParams loginParams) {
        return ssoService.login(loginParams);
    }
}
