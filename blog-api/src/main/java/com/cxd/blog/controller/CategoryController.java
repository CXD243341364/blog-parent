package com.cxd.blog.controller;

import com.cxd.blog.service.CategoryService;
import com.cxd.blog.model.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 23:07
 * @Description: 文章分类
 */
@RestController
@RequestMapping("categorys")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 获取全部文章分类
     *
     * @return
     */
    @GetMapping
    public Result categories() {
        return categoryService.findAll();
    }

    @GetMapping("detail")
    public Result categoryDetail(){
        return categoryService.findAllDetail();
    }

    @GetMapping("detail/{id}")
    public Result categoryDetailById(@PathVariable Long id){
        return categoryService.findDetailById(id);
    }

}
