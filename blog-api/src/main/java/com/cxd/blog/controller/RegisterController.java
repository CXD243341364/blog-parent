package com.cxd.blog.controller;

import com.cxd.blog.service.SsoService;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.params.LoginParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/20 0:20
 * @Description: 注册
 */
@RestController
@RequestMapping("register")
public class RegisterController {

    @Autowired
    private SsoService ssoService;

    @PostMapping
    public Result register(@RequestBody LoginParams loginParams) {
        return ssoService.register(loginParams);
    }
}
