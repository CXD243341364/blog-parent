package com.cxd.blog.model.vo;

import lombok.Data;

/**
 * @author PGTWO
 * @date 2021/9/30 14:56
 * @description：
 */
@Data
public class FileUploadResult extends Result {

    /**
     * 文件唯一标识
     */
    private String uid;

    /**
     * 文件名
     */
    private String name;

    /**
     * 状态有：uploading done error removed
     */
    private String status;

    /**
     * 服务端响应内容，如：'{"status": "success"}'
     */
    private String response;

}
