package com.cxd.blog.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * (Article)表实体类
 *
 * @author cxd
 * @since 2021-08-05 22:13:41
 */
@SuppressWarnings("serial")
@Data
public class Article extends Model<Article> {

    public static final int ARTICLE_TOP = 1;
    public static final int ARTICLE_COMMON = 0;

    private Long id;
    /**
     * 评论数量
     */
    private Integer commentCounts;
    /**
     * 创建时间
     */
    private Long createDate;
    /**
     * 简介
     */
    private String summary;
    /**
     * 标题
     */
    private String title;
    /**
     * 浏览数量
     */
    private Integer viewCounts;
    /**
     * 是否置顶
     */
    private Integer weight;
    /**
     * 作者id
     */
    private Long authorId;
    /**
     * 内容id
     */
    private Long bodyId;
    /**
     * 类别id
     */
    private Long categoryId;

}
