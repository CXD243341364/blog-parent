package com.cxd.blog.model.entity;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/7 23:22
 * @Description:
 */
@Data
public class ArticleBody {

    private Long id;

    /**
     * 文章内容
     */
    private String content;

    /**
     * 文章地址
     */
    private String contentHtml;

    /**
     * 文章ID
     */
    private Long articleId;

}
