package com.cxd.blog.model.vo.params;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/5 23:19
 * @Description: 分页参数
 */
@Data
public class PageParams {

    private int page = 1;

    private int pageSize = 10;

    private Long categoryId;

    private Long tagId;

    /**
     * 当前页数
     */
    private Integer currentPage;

    /**
     * 查询条件
     */
    private String queryString;

    private String year;

    private String month;

    public String getMonth() {
        if (this.month != null && this.month.length() == 1) {
            return "0" + this.month;
        }
        return this.month;
    }
}
