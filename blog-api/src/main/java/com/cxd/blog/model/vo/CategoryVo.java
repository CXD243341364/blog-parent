package com.cxd.blog.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/5 23:38
 * @Description:
 */
@Data
public class CategoryVo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private String avatar;

    /**
     * 类别名
     */
    private String categoryName;

}
