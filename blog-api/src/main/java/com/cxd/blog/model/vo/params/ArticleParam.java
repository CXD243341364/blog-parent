package com.cxd.blog.model.vo.params;

import com.cxd.blog.model.vo.CategoryVo;
import com.cxd.blog.model.vo.TagVo;
import lombok.Data;

import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 23:26
 * @Description:
 */
@Data
public class ArticleParam {

    /**
     * 文章ID
     */
    private Long id;

    /**
     * 文章内容
     */
    private ArticleBodyParam body;

    /**
     * 文章类别
     */
    private CategoryVo category;

    /**
     * 文章概述
     */
    private String summary;

    /**
     * 文章标签
     */
    private List<TagVo> tags;

    /**
     * 文章标题
     */
    private String title;
}
