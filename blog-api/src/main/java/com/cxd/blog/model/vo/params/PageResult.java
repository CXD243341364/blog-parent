package com.cxd.blog.model.vo.params;

import lombok.Data;

import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/10/10 17:32
 * @Description: 分页结果
 */
@Data
public class PageResult<T> {

    private List<T> list;

    private Long total;
}
