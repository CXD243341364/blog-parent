package com.cxd.blog.model.vo;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/9 23:47
 * @Description: 同一错误代码
 */
public enum ResultCode {

    /**=========================用户操作成功代码（20001-2000）================================**/
    ADD_SUCCESS(20001,"插入数据成功"),
    UPDATE_SUCCESS(20002,"更新数据成功"),
    DELETE_SUCCESS(20003,"删除数据成功"),


    /**=========================用户操作错误代码（10001-10005）================================**/
    PARAMS_ERROR(10001,"参数有误"),
    ACCOUNT_PWD_NOT_EXIST(10002,"用户名或密码不存在"),
    TOKEN_NOT_ALLOW(10003,"token不合法"),
    NO_TOKEN(10004,"token不存在"),
    ACCOUNT_PWD_NOT_ALLOW(10005,"用户名命或密码不能为空"),
    ACCOUNT_IS_EXIST(10006,"用户名命或密码已存在"),
    SAVE_USER_INFO_FAILED(10007,"保存用户信息失败"),
    SAVE_USER_INFO_SUCCEEDED(10008,"保存用户信息成功"),
    LOGOUT_CURRENT_USER(10009,"用户登出"),

    /**=========================系统错误代码（70001-90005）================================**/
    ADD_FIELD(80001,"插入数据失败"),
    UPDATE_FIELD(80002,"更新数据失败"),
    DELETE_FIELD(80003,"删除数据失败"),
    NO_PERMISSION(70001,"无访问权限"),
    SESSION_TIME_OUT(90001,"会话超时"),
    NO_LOGIN(90002,"未登录"),;

    private int code;
    private String msg;

    ResultCode(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
