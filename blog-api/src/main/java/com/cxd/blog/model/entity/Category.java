package com.cxd.blog.model.entity;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/7 23:27
 * @Description:
 */
@Data
public class Category {

    private Long id;

    private String avatar;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 分类描述
     */
    private String description;
}
