package com.cxd.blog.model.entity;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 23:40
 * @Description:
 */
@Data
public class ArticleTag {

    private Long id;

    private Long articleId;

    private Long tagId;
}
