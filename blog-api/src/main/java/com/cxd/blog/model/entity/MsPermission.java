package com.cxd.blog.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * (MsPermission)表实体类
 *
 * @author cxd
 * @since 2021-09-09 00:07:24
 */
@SuppressWarnings("serial")
public class MsPermission extends Model<MsPermission> {

    private Long id;

    private String name;

    private String path;

    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
