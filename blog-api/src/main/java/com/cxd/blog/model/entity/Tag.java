package com.cxd.blog.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * (Tag)表实体类
 *
 * @author cxd
 * @since 2021-08-05 22:51:23
 */
@SuppressWarnings("serial")
@Data
public class Tag extends Model<Tag> {

    private Long id;

    private String avatar;

    private String tagName;

}
