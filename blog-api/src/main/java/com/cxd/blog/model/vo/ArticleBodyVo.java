package com.cxd.blog.model.vo;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/5 23:38
 * @Description:
 */
@Data
public class ArticleBodyVo {

    private String content;

}
