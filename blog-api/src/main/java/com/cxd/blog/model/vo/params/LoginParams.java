package com.cxd.blog.model.vo.params;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/9 23:39
 * @Description: 登陆参数
 */
@Data
public class LoginParams {

    private String account;

    private String password;

    private String nickname;
}
