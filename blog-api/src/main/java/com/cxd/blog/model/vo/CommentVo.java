package com.cxd.blog.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 0:39
 * @Description:
 */
@Data
public class CommentVo {

    /**
     * 防止前端精度丢失
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private UserVo author;

    /**
     * 评论
     */
    private String content;

    /**
     * 下一级评论
     */
    private List<CommentVo> childrens;

    private String createDate;

    private Integer leave;

    private UserVo toUser;
}
