package com.cxd.blog.model.dos;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/8 23:28
 * @Description: 文章归档，无需持久化的数据
 */
@Data
public class Articles {

    private Integer year;

    private Integer month;

    private Integer count;
}
