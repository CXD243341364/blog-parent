package com.cxd.blog.model.vo.params;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 22:27
 * @Description:
 */
@Data
public class CommentParam {

    private Long articleId;

    private String content;

    private Long parent;

    /**
     * 被评论用户ID
     */
    private Long toUserId;
}
