package com.cxd.blog.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * (Comment)表实体类
 *
 * @author cxd
 * @since 2021-09-09 00:02:12
 */
@SuppressWarnings("serial")
@Data
public class Comment extends Model<Comment> {

    /**
     * 主键ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 评论
     */
    private String content;

    /**
     * 创建时间
     */
    private Long createDate;

    /**
     * 文章ID
     */
    private Long articleId;

    /**
     * 作者ID
     */
    private Long authorId;

    /**
     * 父评论ID
     */
    private Long parentId;

    /**
     * 对谁评论
     */
    private Long toUid;

    /**
     * 评论级别
     */
    private Integer level;

}
