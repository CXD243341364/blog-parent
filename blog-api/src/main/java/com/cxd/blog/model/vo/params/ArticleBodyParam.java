package com.cxd.blog.model.vo.params;

import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 23:27
 * @Description:
 */
@Data
public class ArticleBodyParam {

    private String content;

    private String contentHtml;
}
