package com.cxd.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxd.blog.model.entity.ArticleTag;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 23:41
 * @Description:
 */
@Mapper
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {
}
