package com.cxd.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxd.blog.model.entity.ArticleBody;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/8 0:00
 * @Description:
 */
@Mapper
public interface ArticleBodyMapper extends BaseMapper<ArticleBody> {
}
