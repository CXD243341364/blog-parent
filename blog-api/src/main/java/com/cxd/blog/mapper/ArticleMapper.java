package com.cxd.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cxd.blog.model.dos.Articles;
import com.cxd.blog.model.entity.Article;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Article)表数据库访问层
 *
 * @author cxd
 * @since 2021-08-05 22:13:41
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 文章归档
     *
     * @return
     */
    List<Articles> listArchives();

    /**
     * 自定义分页
     *
     * @param page
     * @param categoryId
     * @param tagId
     * @param year
     * @param month
     * @return
     */
    IPage<Article> listArticle(Page<Article> page, Long categoryId, Long tagId, String year, String month);

}
