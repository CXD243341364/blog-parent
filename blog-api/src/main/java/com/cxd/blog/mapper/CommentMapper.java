package com.cxd.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxd.blog.model.entity.Comment;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/9/13 0:37
 * @Description:
 */
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {
}
