package com.cxd.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxd.blog.model.entity.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Tag)表数据库访问层
 *
 * @author cxd
 * @since 2021-08-05 22:51:23
 */
@Mapper
public interface TagMapper extends BaseMapper<Tag> {

    /**
     * 根据文章id查询标签列表
     * @param articleId
     * @return
     */
    List<Tag> findTagsByArticleId(Long articleId);

    /**
     * 查询前n条最热标签
     * @param limit
     * @return
     */
    List<Long> findHotsByIds(int limit);

    /**
     * 通过tagId查询tags
     * @param tagIds
     * @return
     */
    List<Tag> findTagsByTagIds(@Param("tagIds") List<Long> tagIds);
}
