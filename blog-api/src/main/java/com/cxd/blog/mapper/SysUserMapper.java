package com.cxd.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxd.blog.model.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/8/5 23:06
 * @Description:
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
