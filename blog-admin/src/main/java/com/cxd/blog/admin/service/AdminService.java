package com.cxd.blog.admin.service;

import com.cxd.blog.admin.model.pojo.Admin;
import com.cxd.blog.admin.model.pojo.Permission;
import com.cxd.blog.model.vo.Result;

import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/10/10 22:52
 * @Description:
 */
public interface AdminService {

    /**
     * 查询权限用户
     *
     * @param username
     * @return
     */
    Admin findAdminByUsername(String username);

    /**
     * 查询用户权限
     *
     * @param adminId
     * @return
     */
    List<Permission> findPermissionsByAdminId(Long adminId);

}