package com.cxd.blog.admin.model.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/10/10 18:13
 * @Description:
 */
@Data
@AllArgsConstructor
public class Admin {

    /**
     * 自增ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    private String username;

    private String password;


}
