package com.cxd.blog.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cxd.blog.admin.mapper.AdminMapper;
import com.cxd.blog.admin.mapper.PermissionMapper;
import com.cxd.blog.admin.model.pojo.Admin;
import com.cxd.blog.admin.model.pojo.Permission;
import com.cxd.blog.admin.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/10/10 22:53
 * @Description:
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private PermissionMapper permissionMapper;


    /**
     * 查询权限用户
     *
     * @param username
     * @return
     */
    @Override
    public Admin findAdminByUsername(String username) {
        LambdaQueryWrapper<Admin> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Admin::getUsername, username);
        queryWrapper.last("limit 1");
        Admin admin = adminMapper.selectOne(queryWrapper);
        return admin;
    }


    /**
     * 查询用户权限
     *
     * @param adminId
     * @return
     */
    @Override
    public List<Permission> findPermissionsByAdminId(Long adminId) {
       return permissionMapper.findPermissionsByAdminId(adminId);
    }
}
