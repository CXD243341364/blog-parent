package com.cxd.blog.admin.service;

import com.cxd.blog.admin.model.pojo.Permission;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.params.PageParams;

/**
 * @author PGTWO
 * @date 2021/9/30 16:01
 * @description：
 */
public interface PermissionService {

    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    Result listPermission(PageParams pageParams);

    /**
     * 增加
     *
     * @param permission
     * @return
     */
    Result add(Permission permission);

    /**
     * 更新
     *
     * @param permission
     * @return
     */
    Result update(Permission permission);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    Result deletePermission(Long id);
}
