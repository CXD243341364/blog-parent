package com.cxd.blog.admin.controller;

import com.cxd.blog.admin.model.pojo.Permission;
import com.cxd.blog.admin.service.PermissionService;
import com.cxd.blog.common.aop.OpLog;
import com.cxd.blog.model.vo.Result;
import com.cxd.blog.model.vo.params.PageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author PGTWO
 * @date 2021/9/30 15:59
 * @description： 权限控制
 */
@RestController
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private PermissionService permissionService;

    @OpLog(operation = "分页查询")
    @PostMapping("permission/permissionList")
    public Result listPermission(@RequestBody PageParams pageParams) {
        return this.permissionService.listPermission(pageParams);
    }

    @PostMapping("permission/add")
    public Result addPermission(@RequestBody Permission permission) {
        return permissionService.add(permission);
    }

    @PostMapping("permission/update")
    public Result updatePermission(@RequestBody Permission permission) {
        return permissionService.update(permission);
    }

    @DeleteMapping("permission/delete/{id}")
    public Result deletePermission(@PathVariable("id") Long id) {
        return permissionService.deletePermission(id);
    }

}
