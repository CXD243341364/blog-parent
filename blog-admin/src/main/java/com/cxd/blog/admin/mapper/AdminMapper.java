package com.cxd.blog.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxd.blog.admin.model.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/10/10 22:53
 * @Description:
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

}
