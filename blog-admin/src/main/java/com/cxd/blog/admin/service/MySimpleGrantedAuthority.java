package com.cxd.blog.admin.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

/**
 * @author PGTWO
 * @date 2021/10/14 17:25
 * @description：
 */
@Service
public class MySimpleGrantedAuthority implements GrantedAuthority {

    private String authority;
    private String path;

    public MySimpleGrantedAuthority(){}

    public MySimpleGrantedAuthority(String authority){
        this.authority = authority;
    }

    public MySimpleGrantedAuthority(String authority,String path){
        this.authority = authority;
        this.path = path;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public String getPath() {
        return path;
    }
}
