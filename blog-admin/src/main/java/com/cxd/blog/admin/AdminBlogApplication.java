package com.cxd.blog.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author PGTWO
 * @date 2021/9/30 15:51
 * @description：
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.cxd.blog"})
public class AdminBlogApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminBlogApplication.class, args);
    }
}
