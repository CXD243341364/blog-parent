package com.cxd.blog.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxd.blog.admin.model.pojo.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/10/10 17:24
 * @Description:
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

    List<Permission> findPermissionsByAdminId(Long adminId);
}
