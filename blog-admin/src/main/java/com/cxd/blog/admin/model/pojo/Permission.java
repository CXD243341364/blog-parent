package com.cxd.blog.admin.model.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: PGTWO
 * @Version: 1.0
 * @Date: 2021/10/10 17:25
 * @Description:
 */
@Data
@AllArgsConstructor
public class Permission {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    /**
     * 请求路径
     */
    private String path;

    /**
     * 描述信息
     */
    private String description;

}
